/**
 * 
 */
package com.parser.webcrawler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author NJ
 * @createdOn 31-Oct-2020
 */
public class WebCrowler {

	public static void main(String[] args) throws MalformedURLException {

		URL url = new URL("https://www.314e.com");
		WebCrowler wc = new WebCrowler(url);
		wc.startCrawling();
		System.out.println("Freq-Word-Collection :: " + wc.getFreqWordCollection(10));
		System.out.println("Freq-Paired-Word-Collection :: " + wc.getFreqPairedWordCollection(10));
		// System.out.println(freqWordCollection);
	}

	WebCrowler(URL url) {

		hostName = url.getHost();
		urls.add(url.toString());

	}

	// configure max URL search limit
	private int maxLevel = 4;

	// track the number of URL can go through
	private int currentLevel = 0;

	// set host name as per the first URL
	private final String hostName;

	Set<String> urls = new LinkedHashSet<>();
	Map<String, FreqWord> freqWordCollection = new LinkedHashMap<>();
	Map<String, FreqPairedWord> freqPairedWordCollection = new LinkedHashMap<>();
	String lastWord = null;

	// initiate crawling
	private void startCrawling() {

		while (maxLevel > currentLevel) {

			lastWord = null;

			URL nextURL = nextURL();

			if (nextURL == null) {
				break;
			}

			htmlParser(nextURL);

			currentLevel++;
		}
	}

	// parse HTML page here
	private void htmlParser(URL url) {

		System.out.println("Start crowling for :: " + url.toString());
		Document doc;
		try {

			doc = Jsoup.connect(url.toString()).get();

			// Get all elements from web page
			Elements elements = doc.getAllElements();

			// Get all links from web page
			Elements links = doc.select("a[href]");

			// check max limit to expand URL search
			if (urls.size() < maxLevel) {
				for (Element link : links) {

					if (urls.size() >= maxLevel) {
						break;
					}
					String linkData = link.attr("abs:href").trim();

					if (linkData.length() == 0)
						continue;

					if (isURL(linkData)) {
						try {
							URL urlAsText = new URL(linkData);

							if (StringUtils.equals(urlAsText.getHost(), hostName)) {
								urls.add(linkData);
								continue;
							}

						} catch (MalformedURLException e) {

							e.printStackTrace();
							return;
						}

					}
				}
			}

			// iterate overall elements and extract data from it
			elements.forEach((element) -> {

				String text = element.ownText();

				if (text == null || text.isEmpty())
					return;

				addFreqWord(text);
				addFreqPairedWord(text);

			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// find out next URL to expand search
	private URL nextURL() {

		int index = 0;

		for (String url : urls) {
			if (index == currentLevel) {

				try {
					return new URL(url);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			index++;
		}

		return null;
	}

	// validate URL
	private boolean isURL(String text) {

		String regex = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)";

		return Pattern.matches(regex, text);
	}

	// maintain frequent pair word count
	private void addFreqPairedWord(String word) {

		String[] wordArr = word.split("\\s");

		for (String text : wordArr) {
			text = text.trim();
			if (text.length() == 0)
				continue;

			if (lastWord == null) {
				lastWord = text;
				continue;
			}
			String pairedWord = lastWord + ' ' + text;
			lastWord = text;

			FreqPairedWord count = freqPairedWordCollection.get(pairedWord);
			if (count == null) {
				freqPairedWordCollection.put(pairedWord, new FreqPairedWord());
			} else {
				count.increment();
			}
		}

	}

	// get highly repeated pair words as per the limit
	public Map<String, Long> getFreqPairedWordCollection(int limit) {

		Map<String, Long> result = freqPairedWordCollection.entrySet().stream()
				.sorted(Map.Entry.<String, FreqPairedWord>comparingByValue(
						Comparator.comparingLong(FreqPairedWord::getCount).reversed()))
				.limit(limit).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getCount(),
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		return result;
	}

	// maintain frequent word count
	private void addFreqWord(String word) {

		String[] wordArr = word.split("\\s");

		for (String text : wordArr) {
			text = text.trim();
			if (text.length() == 0)
				continue;
			FreqWord count = freqWordCollection.get(text);
			if (count == null) {
				freqWordCollection.put(text, new FreqWord());
			} else {
				count.increment();
			}
		}

	}

	// get highly repeated words as per the limit
	public Map<String, Long> getFreqWordCollection(int limit) {

		Map<String, Long> result = freqWordCollection.entrySet().stream()
				.sorted(Map.Entry
						.<String, FreqWord>comparingByValue(Comparator.comparingLong(FreqWord::getCount).reversed()))
				.limit(limit).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getCount(),
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		return result;
	}

}
