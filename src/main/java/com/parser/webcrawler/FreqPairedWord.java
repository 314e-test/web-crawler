/**
 * 
 */
package com.parser.webcrawler;

/**
 * @author NJ
 * @createdOn 31-Oct-2020
 */
public class FreqPairedWord {

	long count = 1;

	public void increment() {
		++count;
	}

	public long getCount() {
		return count;
	}
}
